describe('generar colores en paleta', function() {
    it('generar 2 paletas de colores', function() {
        cy.visit('https://jairobm13.gitlab.io/paletteCreator/palette.html')
        cy.contains("Generar nueva paleta").click()
        cy.screenshot("captura1");
        cy.contains("Generar nueva paleta").click()
        cy.screenshot("captura2");
    });
})