import React, { Component } from 'react';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      reportes: []
    }
  }

  componentDidMount() {
    fetch("/getPrevious", {
      method: "GET",
      headers: { accept: "application/json" }
    }).then((res) => {
      if (res.ok)
        return res.json();
    }).then((reports) => {
      reports.sort((a,b) => {
        var primera = a.fecha.split("_");
        var segunda = b.fecha.split("_");
        return new Date(parseInt(segunda[0]), parseInt(segunda[1]), parseInt(segunda[2]), parseInt(segunda[3]), parseInt(segunda[4]), parseInt(segunda[5])) -new Date(parseInt(primera[0]), parseInt(primera[1]), parseInt(primera[2]), parseInt(primera[3]), parseInt(primera[4]), parseInt(primera[5]));
      });
      console.log(reports)
      this.setState({
        reportes: reports
      }, console.log(this.state.reportes));
    });
  }

  renderInfo(element){
      return (<tr>
        <td>
          <h4>{element.fecha}</h4>
        </td>
        <td>
          <img src={element.img1} />
        </td>
        <td>
          <img src={element.img2} />
        </td>
        <td>
          <img src={element.out} />
        </td>
        <td>
          <p>Tamaño: {element.info.isSameDimensions ? "Iguales" : "Diferentes"}</p>
          <p>Duración analisis: {element.info.analysisTime}</p>
          <p>Porcentaje de diferencia: {element.info.misMatchPercentage}</p>
        </td>
      </tr>);
  }

  generarReporte() {
    fetch("/getReport", {
      method: "GET",
      headers: { accept: "application/json" }
    }).then((res) => {
      if (res.ok)
        return res.json();
    }).then((newReport) => {
      var reports = [];
      reports.push(newReport);
      this.setState({
        reportes: reports.concat(this.state.reportes)
      }, console.log(this.state.reportes));
    });
  }

  render() {
    return (
      <div className="App">
        <button onClick={this.generarReporte.bind(this)}>Generar Reporte</button>
        <table>
          <thead>
            <tr>
              <td>
                Fecha de reporte
              </td>
              <td>
                Imagén original
              </td>
              <td>
                Segunda imagén
              </td>
              <td>
                Comparación
              </td>
              <td>
                Info adicional
              </td>
            </tr>
          </thead>
          <tbody>
            { this.state.reportes !== undefined ? this.state.reportes.map((element) => { return this.renderInfo(element)}) : <tr><td>Nada</td></tr> }
          </tbody>
        </table>
      </div>
    );
  }
}

export default App;
