var express = require('express');
var exec = require('child_process').execSync;
var fs = require('fs');
var resemble = require('resemblejs');

var router = express.Router();

/* GET home page. */
router.get('/getReport', function (req, res, next) {
    exec('cypress run .');
    var fecha = new Date();
    var nombreCarpeta = fecha.getFullYear() + '_' + (fecha.getMonth() + 1) + '_' + fecha.getDate() + '_' + fecha.getHours() + '_' + fecha.getMinutes() + '_' + fecha.getSeconds();
    fs.mkdirSync('./front/build/capturas/' + nombreCarpeta);
    fs.existsSync('./cypress/screenshots/captura1.png')
    fs.copyFileSync('./cypress/screenshots/captura1.png', './front/build/capturas/' + nombreCarpeta + '/captura1.png');
    fs.copyFileSync('./cypress/screenshots/captura2.png', './front/build/capturas/' + nombreCarpeta + '/captura2.png');

    var diff = resemble('./front/build/capturas/' + nombreCarpeta + '/captura1.png')
        .compareTo('./front/build/capturas/' + nombreCarpeta + '/captura2.png').ignoreLess()
        .outputSettings({
            errorType: 'flat',
            largeImageThreshold: 0,
            useCrossOrigin: false,
            outputDiff: true,
            boundingBox: {
                left: 453,
                top: 0,
                right: 1280,
                bottom: 266
            }
        }).onComplete(function (data) {
            var json = JSON.stringify(data);
            fs.writeFileSync('./front/build/capturas/' + nombreCarpeta + '/myJsonFile1.json', json);
            fs.writeFileSync('./front/build/capturas/' + nombreCarpeta + '/output.png', data.getBuffer());
            res.json(
                {
                    fecha : nombreCarpeta,
                    info: data,
                    img1: 'capturas/' + nombreCarpeta + '/captura1.png',
                    img2: 'capturas/' + nombreCarpeta + '/captura2.png',
                    out: 'capturas/' + nombreCarpeta + '/output.png'
                }
            );
        });
});

router.get('/getPrevious', function (req, res, next) {
    var reportes = [];
    fs.readdirSync("./front/build/capturas").forEach(file => {
        var json = fs.readFileSync('./front/build/capturas/'+file+"/myJsonFile1.json");
        var nuevo = {
            fecha : file,
            info: JSON.parse(json),
            img1: 'capturas/' + file + '/captura1.png',
            img2: 'capturas/' + file + '/captura2.png',
            out: 'capturas/' + file + '/output.png'
        }
        reportes.push(nuevo);
    });
    res.json(reportes);
});

module.exports = router;
